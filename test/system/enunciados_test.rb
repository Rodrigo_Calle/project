require "application_system_test_case"

class EnunciadosTest < ApplicationSystemTestCase
  setup do
    @enunciado = enunciados(:one)
  end

  test "visiting the index" do
    visit enunciados_url
    assert_selector "h1", text: "Enunciados"
  end

  test "creating a Enunciado" do
    visit enunciados_url
    click_on "New Enunciado"

    fill_in "Course", with: @enunciado.course_id
    fill_in "Description", with: @enunciado.description
    fill_in "Estado", with: @enunciado.estado
    fill_in "Priority", with: @enunciado.priority_id
    fill_in "Tema", with: @enunciado.tema
    fill_in "User", with: @enunciado.user_id
    click_on "Create Enunciado"

    assert_text "Enunciado was successfully created"
    click_on "Back"
  end

  test "updating a Enunciado" do
    visit enunciados_url
    click_on "Edit", match: :first

    fill_in "Course", with: @enunciado.course_id
    fill_in "Description", with: @enunciado.description
    fill_in "Estado", with: @enunciado.estado
    fill_in "Priority", with: @enunciado.priority_id
    fill_in "Tema", with: @enunciado.tema
    fill_in "User", with: @enunciado.user_id
    click_on "Update Enunciado"

    assert_text "Enunciado was successfully updated"
    click_on "Back"
  end

  test "destroying a Enunciado" do
    visit enunciados_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Enunciado was successfully destroyed"
  end
end

require 'test_helper'

class EnunciadosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enunciado = enunciados(:one)
  end

  test "should get index" do
    get enunciados_url
    assert_response :success
  end

  test "should get new" do
    get new_enunciado_url
    assert_response :success
  end

  test "should create enunciado" do
    assert_difference('Enunciado.count') do
      post enunciados_url, params: { enunciado: { course_id: @enunciado.course_id, description: @enunciado.description, estado: @enunciado.estado, priority_id: @enunciado.priority_id, tema: @enunciado.tema, user_id: @enunciado.user_id } }
    end

    assert_redirected_to enunciado_url(Enunciado.last)
  end

  test "should show enunciado" do
    get enunciado_url(@enunciado)
    assert_response :success
  end

  test "should get edit" do
    get edit_enunciado_url(@enunciado)
    assert_response :success
  end

  test "should update enunciado" do
    patch enunciado_url(@enunciado), params: { enunciado: { course_id: @enunciado.course_id, description: @enunciado.description, estado: @enunciado.estado, priority_id: @enunciado.priority_id, tema: @enunciado.tema, user_id: @enunciado.user_id } }
    assert_redirected_to enunciado_url(@enunciado)
  end

  test "should destroy enunciado" do
    assert_difference('Enunciado.count', -1) do
      delete enunciado_url(@enunciado)
    end

    assert_redirected_to enunciados_url
  end
end

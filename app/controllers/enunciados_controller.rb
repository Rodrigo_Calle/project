class EnunciadosController < ApplicationController
  before_action :set_enunciado, only: [:show, :edit, :update, :destroy ]


  # GET /enunciados
  # GET /enunciados.json
  def index
    @enunciados = Enunciado.where(estado: 1, user_id:current_user).order(created_at: :desc)
  end


  # GET /enunciados/1
  # GET /enunciados/1.json
  def show
    @enunciado = Enunciado.find(params[:id])
  end

  def showReso
    @enunciado = Enunciado.find(params[:id])
  end


  def ListaReso
    @enunciados = Enunciado.where(estado: 1, user_id:current_user).order(created_at: :desc)
    @resolutions =Resolution.where(estado: 1, parametro_id:6 ).order(created_at: :desc)

  
  end
  def listar
    #@resolutions = Resolution.all
    @resolutions =  Resolution.where(estado: 1, user_id:current_user).order(created_at: :desc)
    
  end











  # GET /enunciados/new
  def new
    @enunciado = Enunciado.new
  end

  # GET /enunciados/1/edit

  def edit
    @enunciado = Enunciado.find(params[:id])
  end
  
  
  # POST /enunciados
  # POST /enunciados.json
  def create
    @enunciado = current_user.enunciados.new(enunciado_params)
    respond_to do |format|
      if @enunciado.save
        format.html { redirect_to @enunciado, notice: 'Enunciado fue creado correctamente' }
        format.json { render :show, status: :created, location: @enunciado }
      else
        format.html { render :new }
        format.json { render json: @enunciado.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /enunciados/1
  # PATCH/PUT /enunciados/1.json
  def update
    respond_to do |format|
      if @enunciado.update(enunciado_params)
        @enunciado.estado= 0
        format.html { redirect_to @enunciado, notice: 'Enunciado fue actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @enunciado }
      else
        format.html { render :edit }
        format.json { render json: @enunciado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /enunciados/1
  # DELETE /enunciados/1.json
  def destroy
    respond_to do |format|
      @enunciado.update estado: 0
  
        format.html { redirect_to enunciados_url, notice: 'Enunciado fue elimado correctamente' }
        format.json { render :enunciados, status: :ok, location: @enunciado }
    
  end
end

def destroy2
  @enunciado.destroy
  respond_to do |format|
    format.html { redirect_to enunciados_url, notice: 'Enunciado fue elimado correctamente' }
    format.json { head :no_content }
  end
end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enunciado
      @enunciado = Enunciado.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def enunciado_params
      params.require(:enunciado).permit(:tema, :description, :estado, :course_id, :priority_id, :user_id,:avatar , :parametro_id)
    end
  

end

class ResolutionsController < ApplicationController
  before_action :set_resolution, only: [:show, :edit, :update, :destroy]

  # GET /resolutions
  # GET /resolutions.json
  def index
    @resolutions = Resolution.all
    @enunciados = Enunciado.where(estado: 1, parametro_id:4).order(created_at: :desc)
  end

  def showReso2
    @enunciado = Enunciado.find(params[:id])
  end
  def listar
    #@resolutions = Resolution.all
    @resolutions =  Resolution.where(estado: 1, user_id:current_user).order(created_at: :desc)
    
  end
  # GET /resolutions/1
  # GET /resolutions/1.json
  def show
    @resolutions = Resolution.find(params[:id])
  end

  # GET /resolutions/new
  def new
    @resolution = Resolution.new
  end

  # GET /resolutions/1/edit
  def edit
    @resolution = Resolution.find(params[:id])
  end

  # POST /resolutions
  # POST /resolutions.json
  def create
    @enunciado = Enunciado.find(params[:enunciado_id])
    @resolution = @enunciado.resolutions.new(resolution_params)
    @resolution.user = current_user  
    #@resolution.create  user_id:current_user
    @enunciado.update  parametro_id:6
    #redirect_to @enunciados_path(@nunciados)
    respond_to do |format|
      if @resolution.save
        format.html { redirect_to @resolution, notice: 'Resolution was successfully created.' }
        format.json { render :show, status: :created, location: @resolution }
      else
        format.html { render :new }
        format.json { render json: @resolution.errors, status: :unprocessable_entity }
      end
    end
  end

   
    


  def create2
    @resolution = Resolution.new(resolution_params)

    respond_to do |format|
      if @resolution.save
        format.html { redirect_to @resolution, notice: 'Resolution was successfully created.' }
        format.json { render :show, status: :created, location: @resolution }
      else
        format.html { render :new }
        format.json { render json: @resolution.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    respond_to do |format|
      @enunciado.create parametro_id: 5
  
        format.html { redirect_to resolution_url, notice: 'Enunciado fue elimado correctamente' }
        format.json { render :resolutions, status: :ok, location: @resolution }
    
  end
end


  # PATCH/PUT /resolutions/1
  # PATCH/PUT /resolutions/1.json
  def update
    respond_to do |format|
      if @resolution.update(resolution_params)
        format.html { redirect_to @resolution, notice: 'Resolution was successfully updated.' }
        format.json { render :show, status: :ok, location: @resolution }
      else
        format.html { render :edit }
        format.json { render json: @resolution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resolutions/1
  # DELETE /resolutions/1.json
  def destroy2
    @resolution.destroy
    respond_to do |format|
      format.html { redirect_to resolutions_url, notice: 'Resolution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resolution
      @resolution = Resolution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def resolution_params
      params.require(:resolution).permit(:puntaje, :comentario, :estado, :parametro_id, :enunciado_id, :user_id,:avatar)
    end
end

CheckoutJS = {
    getBaseUrl: function() {
        var getUrl = window.location;
        return getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/";
    },

    configCheckout: function() {        
        Culqi.publicKey = 'pk_test_FWghDVG8yNg9Oq62';    
        Culqi.options({
            lang: 'es',
            modal: true,
            style: {
              logo: 'https://upc.edu.pe/static/img/logo_upc_red.png',
              maincolor: '#FA0107',
              buttontext: '#ffffff',
              maintext: '#4A4A4A',
              desctext: '#4A4A4A'
            }
        });
        Culqi.settings({
            title: 'Final UPC',
            currency: 'PEN',
            description: 'Recarga de monedero',
            amount: 0
        });
    },

    mostrarMensaje: function(mensaje) {
        $("#spn_alert_monto_recarga").html(mensaje);
        $("#spn_alert_monto_recarga").css("display","block");
        $(".alert").css("display","block");
    },

    ocultarMensaje: function() {
        $("#spn_alert_monto_recarga").html("");
        $("#spn_alert_monto_recarga").css("display","none");
        $(".alert").css("display","none");
    },

    realizarPago: function (e) {
        this.ocultarMensaje();
        // Abre el formulario con las opciones de Culqi.settings

        var montoRecarga = parseFloat($("#inputMonto").val());
        console.log(montoRecarga)
        if (!this.esNumero(montoRecarga)) {
            return false;
        }
        Culqi.settings({
            title: 'Final UPC',
            currency: 'PEN',
            description: 'Recarga de monedero',
            amount: (montoRecarga * 100)
        });
        Culqi.open();
        e.preventDefault();

    },

    esNumero: function (numero) {
        if (isNaN(numero) || numero < 3 || numero > 9999) {
            this.mostrarMensaje("No es un monto de recarga válido!");
            return false;
        }
        return true;        
    },

    limpiarForm: function () {
        $("#inputMonto").val("");
    },

    actualizaSaldo: function (saldo) {
        $("#spn_saldo_actual").html("S/ "+ (saldo != null ? saldo.toFixed(2) : "") );
    }
};

var checkoutJS = CheckoutJS;
checkoutJS.configCheckout();


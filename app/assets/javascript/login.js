LoginJS = {
	validarFormularioNuevoUsuario: function(event) {
		var email = $("#txt_email").val().trim();
		var nombres = $("#txt_nombres").val().trim();
		var apellidos = $("#txt_apellidos").val().trim();
		var password = $("#txt_password").val().trim();
		var password_confirm = $("#txt_password_confirm").val().trim();

		if (email === "") {		
			this.mostrarMensajeWarning("Campo email es incorrecto!");	
			return false;
		}
		if (nombres === "") {	
			this.mostrarMensajeWarning("Campo nombres es incorrecto!");		
			return false;
		}
		if (apellidos === "") {	
			this.mostrarMensajeWarning("Campo apellidos es incorrecto!");		
			return false;
		}
		if (password === "" || password.length < 7 || password.length > 30 ) {	
			this.mostrarMensajeWarning("La contraseña debe contener entre 7 y 30 caracteres!");		
			return false;
		}
		if (password !== password_confirm) {	
			this.mostrarMensajeWarning("Las contraseñas ingresadas no coinciden!");
			return false;
		}
		$("#form_registro").submit(); 
	},

	validarFormularioDatos: function(event) {
		$("#form_perfil").submit(); 
	},

	validarFormularioActualizarContrasena: function(event) {
		var password_rc = $("#password_rc").val().trim();
		var password_confirm_rc = $("#password_confirm_rc").val().trim();
		var id_usuario_rc = $("#hid_id_usuario_rc").val().trim();
		var code_rc = $("#hid_code_rc").val().trim();

		if (id_usuario_rc === "") {		
			this.mostrarMensajeWarning("No se pudo obtener el usuario desde el link de recuperar contraseña!");	
			return false;
		}
		if (code_rc === "") {	
			this.mostrarMensajeWarning("No se pudo obtener el código desde el link de recuperar contraseña!");		
			return false;
		}
		if (password_rc === "" || password_rc.length < 7 || password_rc.length > 30 ) {	
			this.mostrarMensajeWarning("La contraseña debe contener entre 7 y 30 caracteres!");		
			return false;
		}
		if (password_rc !== password_confirm_rc) {	
			this.mostrarMensajeWarning("Las contraseñas ingresadas no coinciden!");
			return false;
		}
		$("#form_recuperar_contrasena").submit(); 
	},

	mostrarMensajeWarning: function(mensaje) {		
		$("#div_alert").removeClass();
		$("#div_alert").addClass("alert alert-warning");
		$("#spn_nuevo_usuario").html(mensaje);
		$("#spn_nuevo_usuario").css("display","block");
		$(".alert").css("display","block");
	},

	mostrarMensajeSuccess: function(mensaje) {	
		$("#div_alert").removeClass();
		$("#div_alert").addClass("alert alert-success");
		$("#spn_nuevo_usuario").html(mensaje);
		$("#spn_nuevo_usuario").css("display","block");
		$(".alert").css("display","block");
	},

	ocultarMensaje: function() {
		$("#spn_nuevo_usuario").html("");
		$("#spn_nuevo_usuario").css("display","none");
		$(".alert").css("display","none");
	},

	onclicForgotPassword: function() {
		$("#form_iniciar_sesion").hide('slow');
		$("#form_forgot_password").show('slow');
	},

	onclicForgotPasswordCancel: function() {
		$("#form_iniciar_sesion").show('slow');
		$("#form_forgot_password").hide('slow');		
	},

	validarFormularioEditPassword: function(event) {

		var txt_password = $("#txt_password").val().trim();
		var txt_new_password = $("#txt_new_password").val().trim();
		var txt_new_password_confirm = $("#txt_new_password_confirm").val().trim();

		if (txt_password === "") {		
			this.mostrarMensajeWarning("Debe ingresar contraseña actual!");	
			return false;
		}
		console.log("1")
		console.log("txt_new_password" + txt_new_password)
		if (txt_new_password === "") {	
			this.mostrarMensajeWarning("Debe ingresar la nueva contraseña!");		
			return false;
		}
		console.log("2")
		if (txt_new_password === "" || txt_new_password.length < 7 || txt_new_password.length > 30 ) {	
			this.mostrarMensajeWarning("La contraseña debe contener entre 7 y 30 caracteres!");		
			return false;
		}
		console.log("3")
		if (txt_new_password_confirm === "") {	
			this.mostrarMensajeWarning("Debe ingresar la confirmación de la nueva contraseña!");		
			return false;
		}
		console.log("4")
		if (txt_new_password_confirm === "" || txt_new_password_confirm.length < 7 || txt_new_password_confirm.length > 30 ) {	
			this.mostrarMensajeWarning("La nueva contraseña debe contener entre 7 y 30 caracteres!");		
			return false;
		}
		console.log("5")
		if(txt_new_password !== txt_new_password_confirm){
			this.mostrarMensajeWarning("Las nuevas contraseñas no coinciden!");		
			return false;
		}
		$("#form_password").submit(); 
	},

};
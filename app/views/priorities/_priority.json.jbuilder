json.extract! priority, :id, :description, :costo, :descosto, :idestado, :created_at, :updated_at
json.url priority_url(priority, format: :json)

json.extract! resolution, :id, :puntaje, :comentario, :estado, :parametro_id, :enunciado_id, :user_id, :created_at, :updated_at
json.url resolution_url(resolution, format: :json)

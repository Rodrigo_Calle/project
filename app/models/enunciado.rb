class Enunciado < ApplicationRecord
  validates :tema , :description ,:avatar,     presence:{message: "Obligatorio"}
  
  belongs_to :course
  belongs_to :priority
  belongs_to :user
  belongs_to :parametro
  has_one_attached :avatar
  has_many :resolutions

  #before_save :default_values 
  #def default_values 
    #self.estado ||= 1
   #end

   before_save :default_values 
   def default_values 
   self.estado ||= 1
   
   end
  
end

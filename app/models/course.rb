class Course < ApplicationRecord
    validates :description,  presence:{message: "Obligatorio"}
    has_many :enunciados
    
    before_save :default_values 
    def default_values 
    self.estado ||= 1
   end

end

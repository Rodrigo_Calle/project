class Resolution < ApplicationRecord

  validates :parametro_id , :comentario ,:avatar,     presence:{message: "Obligatorio"}
  
  belongs_to :parametro
  belongs_to :enunciado
  belongs_to :user
  has_one_attached :avatar

  before_save :default_values 
  def default_values 
    self.estado ||= 1
   end
end

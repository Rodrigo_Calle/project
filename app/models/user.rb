class User < ApplicationRecord
  has_many :enunciados
  has_many :resolutions
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
         validates :name, presence: true
         validates :apellido, presence: true
         validates :dni, presence: true, length: { is: 8 } 
         validates :apellido, presence: true
         validates :born_on, presence: true
         validates :phone, presence: true, length: { is: 9 }

        
      
         
end

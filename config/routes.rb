Rails.application.routes.draw do
  resources :resolutions
  resources :parametros
  get 'saldos/recagas'
  get 'saldos/movimientos'
  
  get 'saldos/recagas'
  get 'saldos/movimientos'
  resources :enunciados do
  resources :resolutions
  end
  resources :priorities
  resources :courses
  resources :movimiento_saldo, only: [:index]
root to:  'pages#home'
get "enunciados/:id" => "enunciados#show2"
get "resolutions" => "resolutions#listar"
resources :resolutions
get 'listar'  , to: "resolutions#listar"
get 'showReso/:id', to: "enunciados#showReso"
get 'showReso2/:id', to: "resolutions#showReso2"
get 'ListaReso', to: "enunciados#ListaReso"
  devise_for :users#, controllers: { registrations: "registrations"}
  #as :user do
   # get '/' => 'devise/registrations#new'
  #end 
    
  resources :pages
 get 'profile_teacher', to: "pages#profile_teacher"
 get 'profile_student', to: "pages#profile_student"
  get 'profile_administrator', to: "pages#profile_administrator"
 get 'edit_profile', to: "pages#edit_profile"

 get '*path'=> redirect('/')
 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

class CreateResolutions < ActiveRecord::Migration[6.0]
  def change
    create_table :resolutions do |t|
      t.text :puntaje
      t.text :comentario
      t.integer :estado
      t.references :parametro, null: false, foreign_key: true
      t.references :enunciado, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

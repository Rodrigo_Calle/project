class CreatePriorities < ActiveRecord::Migration[6.0]
  def change
    create_table :priorities do |t|
      t.text :description
      t.decimal :costo
      t.text :descosto
      t.integer :idestado

      t.timestamps
    end
  end
end

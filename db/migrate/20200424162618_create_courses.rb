class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.text :description
      t.integer :estado

      t.timestamps
    end
  end
end

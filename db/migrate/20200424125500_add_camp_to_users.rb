class AddCampToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :tipo, :string
    add_column :users, :dni, :string
    add_column :users, :name, :string
    add_column :users, :apellido, :string
    add_column :users, :born_on, :string
    add_column :users, :phone, :string
    add_column :users, :sexo, :string

    add_column :users, :confirmation_token, :string
    add_column :users, :confirmed_at, :string
    add_column :users, :confirmation_sent_at, :string
    add_column :users, :unconfirmed_email, :string

    
  end
end

class CreateParametros < ActiveRecord::Migration[6.0]
  def change
    create_table :parametros do |t|
      t.integer :grupo
      t.integer :codigo
      t.text :description
      t.integer :estado

      t.timestamps
    end
  end
end

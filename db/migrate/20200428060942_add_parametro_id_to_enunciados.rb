class AddParametroIdToEnunciados < ActiveRecord::Migration[6.0]
  def change
   
    add_reference :enunciados, :parametro, foreign_key: true
    
  end
end

class CreateEnunciados < ActiveRecord::Migration[6.0]
  def change
    create_table :enunciados do |t|
      t.text :tema
      t.text :description
      t.integer :estado
      t.references :course, null: false, foreign_key: true
      t.references :priority, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

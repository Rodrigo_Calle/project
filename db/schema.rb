# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_28_151633) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "courses", force: :cascade do |t|
    t.text "description"
    t.integer "estado"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "enunciados", force: :cascade do |t|
    t.text "tema"
    t.text "description"
    t.integer "estado"
    t.integer "course_id", null: false
    t.integer "priority_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "parametro_id"
    t.index ["course_id"], name: "index_enunciados_on_course_id"
    t.index ["parametro_id"], name: "index_enunciados_on_parametro_id"
    t.index ["priority_id"], name: "index_enunciados_on_priority_id"
    t.index ["user_id"], name: "index_enunciados_on_user_id"
  end

  create_table "parametros", force: :cascade do |t|
    t.integer "grupo"
    t.integer "codigo"
    t.text "description"
    t.integer "estado"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "priorities", force: :cascade do |t|
    t.text "description"
    t.decimal "costo"
    t.text "descosto"
    t.integer "idestado"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "resolutions", force: :cascade do |t|
    t.text "puntaje"
    t.text "comentario"
    t.integer "estado"
    t.integer "parametro_id", null: false
    t.integer "enunciado_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["enunciado_id"], name: "index_resolutions_on_enunciado_id"
    t.index ["parametro_id"], name: "index_resolutions_on_parametro_id"
    t.index ["user_id"], name: "index_resolutions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "tipo"
    t.string "dni"
    t.string "name"
    t.string "apellido"
    t.string "born_on"
    t.string "phone"
    t.string "sexo"
    t.string "confirmation_token"
    t.string "confirmed_at"
    t.string "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "enunciados", "courses"
  add_foreign_key "enunciados", "parametros"
  add_foreign_key "enunciados", "priorities"
  add_foreign_key "enunciados", "users"
  add_foreign_key "resolutions", "enunciados"
  add_foreign_key "resolutions", "parametros"
  add_foreign_key "resolutions", "users"
end
